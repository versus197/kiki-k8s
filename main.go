/*
Copyright 2016 Valentin V. Nastenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"k8s.io/client-go/1.4/kubernetes"
	"k8s.io/client-go/1.4/pkg/api"
	v1 "k8s.io/client-go/1.4/pkg/api/v1"
	"k8s.io/client-go/1.4/tools/clientcmd"
)

var (
	kubeconfig = flag.String("kubeconfig", "./config", "absolute path to the kubeconfig file")
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	flag.Parse()
	// uses the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	check(err)

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	check(err)

	qSrv := &v1.Service{
		ObjectMeta: v1.ObjectMeta{
			Name: "servicefirst",
		},
		Spec: v1.ServiceSpec{
			Type:     v1.ServiceTypeClusterIP,
			Selector: map[string]string{"app": "bservice-v1-0-3"},
			Ports: []v1.ServicePort{
				v1.ServicePort{
					Protocol: v1.ProtocolTCP,
					Port:     80,
				},
			},
		},
	}

	qPod := &v1.Pod{
		Status: v1.PodStatus{
			Phase: v1.PodRunning,
		},
		ObjectMeta: v1.ObjectMeta{
			Labels: map[string]string{
				"foo":  "bar",
				"name": "baz",
			},
			Name: "my-pod",
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Name:  "nginx-go",
					Image: "nginx",
					Ports: []v1.ContainerPort{
						{
							ContainerPort: 80,
						},
					},
				},
			},
		},
	}

	clientset.Core().Services(api.NamespaceDefault).Delete("servicefirst", &api.DeleteOptions{})
	//check(err)

	go func() {
		clientset.Core().Pods(api.NamespaceDefault).Delete("my-pod", &api.DeleteOptions{})
		time.Sleep(10 * time.Second)
		pod, err := clientset.Core().Pods(api.NamespaceDefault).Create(qPod)
		check(err)
		pod.SetLabels(map[string]string{
			"pod-group": "my-pod-group",
		})
		pod, err = clientset.Core().Pods(api.NamespaceDefault).Update(pod)
		check(err)
	}()

	//check(err)

	srv, err := clientset.Core().Services(api.NamespaceDefault).Create(qSrv)
	check(err)

	srv.SetLabels(map[string]string{
		"pod-group": "my-pod-group",
	})
	srv, err = clientset.Core().Services(api.NamespaceDefault).Update(srv)
	check(err)

	for {
		pods, err := clientset.Core().Pods(api.NamespaceDefault).List(api.ListOptions{})
		check(err)
		s, err := clientset.Core().Services(api.NamespaceDefault).Get("bservice-v1-0-3-service")
		check(err)
		fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))
		fmt.Println("Name:", s.Name)
		for p, _ := range s.Spec.Ports {
			fmt.Println("Port:", s.Spec.Ports[p].Port)
			fmt.Println("NodePort:", s.Spec.Ports[p].NodePort)
		}
		time.Sleep(10 * time.Second)
	}
}
